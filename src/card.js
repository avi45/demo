import React from "react";
import { View, TouchableOpacity, Text } from 'react-native'
import styles from './style'
const Card = ({ onClick, card, index, isInactive, isFlipped, isDisabled }) => {
    const handleClick = () => {
        !isFlipped && !isDisabled && onClick(index);
    };
    return (
        <View style={styles.cardWrapper}>
            <TouchableOpacity
                onPress={handleClick}
                disabled={isInactive}
                style={[styles.card, { backgroundColor: (isFlipped || isInactive) ? "#fff" : '#8EC2CD' }]}>
                {(isFlipped || isInactive) && <Text style={styles.letter}>{card.type}</Text>}
            </TouchableOpacity>
        </View>
    );
};

export default Card;
