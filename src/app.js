

import React, { useEffect, useState, useRef } from "react";
import { SafeAreaView, View, FlatList, Text } from 'react-native'
import styles from './style'
import Card from './card'
const uniqueCardsArray = [
    {
        type: "A",
    },
    {
        type: "B",
    },
    {
        type: "C",
    },
    {
        type: "D",
    },
    {
        type: "E",
    },
    {
        type: "F",
    },
    {
        type: "G",
    },
    {
        type: "H",
    }
];

function shuffleCards(array) {
    const length = array.length;
    for (let i = length; i > 0; i--) {
        const randomIndex = Math.floor(Math.random() * i);
        console.log("Random index", randomIndex)
        const currentIndex = i - 1;
        const temp = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temp;
    }
    return array;
}

export default function App() {
    const [cards, setCards] = useState(() =>
        shuffleCards(uniqueCardsArray.concat(uniqueCardsArray))
    );
    const [openCards, setOpenCards] = useState([]);
    const [clearedCards, setClearedCards] = useState({});
    const [shouldDisableAllCards, setShouldDisableAllCards] = useState(false);
    const [moves, setMoves] = useState(0);
    const [showModal, setShowModal] = useState(false);

    const timeout = useRef(null);

    const disable = () => {
        setShouldDisableAllCards(true);
    };
    const enable = () => {
        setShouldDisableAllCards(false);
    };


    const evaluate = () => {
        const [first, second] = openCards;
        enable();
        if (cards[first].type === cards[second].type) {
            setClearedCards((prev) => ({ ...prev, [cards[first].type]: true }));
            setOpenCards([]);
            return;
        }
        // This is to flip the cards back after 500ms duration
        timeout.current = setTimeout(() => {
            setOpenCards([]);
        }, 500);
    };
    const handleCardClick = (index) => {
        if (openCards.length === 1) {
            setOpenCards((prev) => [...prev, index]);
            setMoves((moves) => moves + 1);
            disable();
        } else {
            clearTimeout(timeout.current);
            setOpenCards([index]);
        }
    };

    useEffect(() => {
        let timeout = null;
        if (openCards.length === 2) {
            timeout = setTimeout(evaluate, 300);
        }
        return () => {
            clearTimeout(timeout);
        };
    }, [openCards]);



    const checkIsFlipped = (index) => {
        if (openCards.includes(index)) {
        }
        return openCards.includes(index);
    };

    const checkIsInactive = (card) => {
        return Boolean(clearedCards[card.type]);
    };


    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={cards}
                numColumns={4}
                scrollEnabled={false}
                keyExtractor={(item, index) => item.type + index}
                renderItem={(item, index) => <Card
                    key={item.type + index}
                    card={item.item}
                    index={item.index}
                    isDisabled={shouldDisableAllCards}
                    isInactive={checkIsInactive(item.item)}
                    isFlipped={checkIsFlipped(item.index)}
                    onClick={handleCardClick}
                />
                }
            />
            <View style={{ marginBottom: 50, marginLeft: 25 }}>
                <Text style={{ lineHeight: 35, fontSize: 18, fontWeight: 'bold', color: '#000' }}>Attempt Count: {moves}</Text>
            </View>
        </SafeAreaView>

    );
}
