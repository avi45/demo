import { StyleSheet } from 'react-native'
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E1DAD8'
    },
    cardWrapper: {
        height: 100,
        paddingVertical: 10,
        paddingHorizontal: 10,
        width: "25%",
        backgroundColor: '#EBC6BC'
    },
    card: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    letter:{
        fontSize: 25,
        fontWeight: 'bold',
        color: '#000'
    }

})

export default styles